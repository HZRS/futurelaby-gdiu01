using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particles : MonoBehaviour
{
    public ParticleSystem P;
    public GameObject Steam;
    public GameObject Gloop;
    private void Start()
    {
        P = GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag== "Water")
        {
            P.Play();
            Steam.SetActive(false);
            Gloop.SetActive(false);
        }
    }
}

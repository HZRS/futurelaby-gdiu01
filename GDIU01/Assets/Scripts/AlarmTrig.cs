using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
public class AlarmTrig : MonoBehaviour
{
    public AudioSource s = new AudioSource();
    public AudioClip c;
    public void Awake()
    {
        s = GetComponent<AudioSource>();
    }
    public void OnTriggerEnter(Collider other)
    {
        s.Play();
    }
}

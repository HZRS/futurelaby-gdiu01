using UnityEngine;
using UnityEngine.Playables;

public class GenericTrigger : MonoBehaviour 
{
	public PlayableDirector timeline;

	// Use this for initialization
	void Start() 
	{
		timeline = GetComponent<PlayableDirector>();
	}
		

	void OnTriggerExit(Collider c)
	{
		if (c.gameObject.tag == "Player") 
		{
			timeline.Pause();
		}
	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag == "Player")
		{
			timeline.Play();
		}
	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectTrigger : MonoBehaviour
{
    public GameObject FirstObject;
    public GameObject SecondObject;
   



    void OnTriggerEnter(Collider other)
    {
        StartCoroutine(W());
        
        StartCoroutine(E());
        
    }
    IEnumerator W()
    {
        yield return new WaitForSeconds(0.6f);
        FirstObject.SetActive(true);
        SecondObject.SetActive(true);
       
    }
    IEnumerator E()
    {
        yield return new WaitForSeconds(12.4f);
        FirstObject.SetActive(false);
        SecondObject.SetActive(false);
       
    }
}

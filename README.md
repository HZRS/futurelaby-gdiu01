# GDIU01
This is a 3D Unity project that helps to understand the fundamentals and tools of Unity in order to begin the journey of 3D Game Development.
Using the main simple tools we have created a simple warehouse scene which should cover most of the basic ideas that worry a beginner.

## How to start with GDIU01?
You can start by downloading the project, take a look inside, test the gameplay and try to get the idea out of it.
The next step you should start your own project and try to build your scene from scratch using the assets provided.<br />
**-** Try to write your own scripts.<br />
**-** Try to be creative using the assets to improve your scene.<br />
**-** Testing new ideas on the scene will increase your level of experience.<br />
**-** Building the scene piece by peice is always better for beginners.<br />

## About The Assets
We've collected the proper free assets packs we found on the assets store and it worth mentioning that there are many other platforms to get free assets anyone can find on internet.<br />
It is important to **Note** that you should search for new assets and try to use them instead of the provided ones as it is improtant to have the ability to search and cover your needs.

Assets Packs uesd in this project:<br />
**-** [Standard Assets (for Unity 2018.4)](https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-for-unity-2018-4-32351#publisher).<br />
**-** [Sci-Fi Construction Kit (Modular)](https://assetstore.unity.com/packages/3d/environments/sci-fi/sci-fi-construction-kit-modular-159280).<br />
**-** [Sci Fi Doors](https://assetstore.unity.com/packages/3d/environments/sci-fi/sci-fi-doors-162876).<br />
**-** [Free PBR Barrel](https://assetstore.unity.com/packages/3d/props/free-pbr-barrel-108702).<br />
**-** [Metal Floor (Rust Low) Texture](https://assetstore.unity.com/packages/2d/textures-materials/metals/metal-floor-rust-low-texture-40351).<br />
**-** [Fork Loader HQ](https://assetstore.unity.com/packages/3d/vehicles/land/fork-loader-hq-123806).<br />

## Topics covered
In this project you can use this project as a start point for your game or to learn about:<br />
**-** Working with Assets.<br />
**-** Applying Materials.<br />
**-** Prefabs.<br />
**-** Animation.<br />
**-** Collisions.<br />
**-** Audio.<br />
**-** Lighting.<br />
**-** Particles and FX.<br />
**-** Post Processing.<br />
**-** Timeline and Cinematics.<br />
**-** Charecter control (Third Person Controller).<br />

## About the creator
This project was created by **"Hasan Zidan"** as a Game Development Internship assignment and uploaded in 2022 to be archived in a try to help beginners that can make use of it.<br />

**Internship provider:** [Future LabY](https://futurelaby.com/) a startup working on educational AR/VR applications.<br />
**Mentor:** [Taher Hassan](https://www.linkedin.com/in/taher-hassan/) CTO at FuturLabY and Game Development Team Leader.<br />


## License
[MIT](https://choosealicense.com/licenses/mit/)